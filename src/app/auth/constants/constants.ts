import { HttpHeaders } from "@angular/common/http";

export const AUTH_API = 'http://localhost:3000/';
export const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
}
export const TOKEN_KEY = 'auth-token'
export const USER_KEY = 'auth-user'
export const TOKEN_HEADER_KEY = 'x-access-token'