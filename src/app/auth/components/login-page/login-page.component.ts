import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { TokenStorageService } from '../../services/token-storage.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  form: FormGroup;
  submitted: boolean = false;

  constructor(
    private authService: AuthService,
    private tokenStorageService: TokenStorageService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group(
      {
        username: ['', [Validators.required]],
        password: ['', [Validators.required]]
      },

    )
  }

  get f(): { [key: string]: AbstractControl } {
    return this.form.controls
  }

  onSubmit(): void {
    const { username, password } = this.form.value;
    this.submitted = true;
    this.authService.login(username, password).subscribe(
      data => {
        console.log(data);
        this.tokenStorageService.saveToken(data.access_token);
        this.tokenStorageService.saveUser(data.user);
        this.router.navigate(['']).then(() => {
          this.reloadPage()
        })
      },
      err => {
        console.log(err.errror.message);
        
      }
    )
  }

  reloadPage() {
    window.location.reload();
  }
}
