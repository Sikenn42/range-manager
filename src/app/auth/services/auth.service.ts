import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { shareReplay, tap } from 'rxjs/operators';
import { AUTH_API, httpOptions } from '../constants/constants';
import { User } from '../models/user.interface';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  login(username: string, password: string): Observable<any> {
    return this.http.post(AUTH_API + 'auth/login', {
      username,
      password,
    }, httpOptions)
  }

  register(username: string, email: string, password: string): Observable<any> {
    return this.http.post(AUTH_API + 'users', {
      username: username,
      email: email,
      password: password
    }, httpOptions)
  }
}
