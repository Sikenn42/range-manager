import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from './auth/components/login-page/login-page.component';
import { SignInPageComponent } from './auth/components/sign-in-page/sign-in-page.component';
import { EditorPageComponent } from './editor/components/editor-page/editor-page.component';
import { LandingPageComponent } from './shared/components/landing-page/landing-page.component';
import { PageNotFoundComponent } from './shared/components/page-not-found/page-not-found.component';
import { ViewPageComponent } from './view/components/view-page/view-page.component';

const routes: Routes = [
  { path: '', component: LandingPageComponent},
  { path: 'login', component: LoginPageComponent },
  { path: 'sign-in', component: SignInPageComponent},
  { path: 'editor', component: EditorPageComponent},
  { path: 'view', component: ViewPageComponent},
  { path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
