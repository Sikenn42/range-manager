import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewPageComponent } from './components/view-page/view-page.component';



@NgModule({
  declarations: [ViewPageComponent],
  imports: [
    CommonModule
  ],
  exports: [ViewPageComponent]
})
export class ViewModule { }
