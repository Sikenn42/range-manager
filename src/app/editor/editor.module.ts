import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditorPageComponent } from './components/editor-page/editor-page.component';



@NgModule({
  declarations: [EditorPageComponent],
  imports: [
    CommonModule
  ],
  exports: [EditorPageComponent]
})
export class EditorModule { }
